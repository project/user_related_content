<?php
/**
 *  Create a relationship
 */
function user_related_content_create_relationship($params) {
  // delete first just in case it already happens to exist
  user_related_content_delete_relationship($params);
  db_query('INSERT INTO {user_related_content_relationships} (rtid, nid, uid) VALUES (%d, %d, %d)', $params['rtid'], $params['nid'], $params['uid']);
  cache_clear_all();
}

/**
 *  Delete a relationship
 */
function user_related_content_delete_relationship($param = NULL) {

  $query = 'DELETE FROM {user_related_content_relationships} ';
  $query .= _user_related_content_param_sql($param, 'r', FALSE, FALSE);
  db_query($query, $param);
  cache_clear_all();
}

/**
 *  Get relationships
 *
 *  @param $param
 *  Constraints which the returned relationships must satisfy.  Valid keys are
 *  'uid', 'nid', 'rtid' and 'count'.
 *
 *  @param $paged
 *  If set to TRUE, then the query is paged.
 *
 *  @return
 *  Array of relationship objects satisfying the $param constraints
 */
function user_related_content_get_relationships($param, $paged = FALSE) {

  $query = 'SELECT r.* FROM {user_related_content_relationships} r ';
  $query .= _user_related_content_param_sql($param, 'r');
  
  if (!$paged) {
    $result = db_query($query, $param);
  }
  else {
    $result = pager_query($query, variable_get('default_nodes_main', 10), 0, NULL, $param); 
  }

  $relationships = array();
  while ($relationship = db_fetch_object($result)) {  
    $relationships[] = $relationship;
  }
  
  return $relationships;
}

/**
 *  Creates a new relationship_type with the given parameters
 */
function user_related_content_create_relationship_type($data) {

  if (isset($data['rtid'])) {
    $rtid = $data['rtid'];
  }
  else {
    $rtid = db_next_id('user_related_content_relationship_types');
  }
  
  db_query("INSERT INTO {user_related_content_relationship_types} (rtid, title, description, desc1, desc2) VALUES (%d, '%s', '%s', '%s', '%s')", $rtid, $data['title'], $data['description'], $data['desc1'], $data['desc2']);
      
  foreach ($data['content_types'] AS $content_type) {
    db_query("INSERT INTO {user_related_content_relationship_type_content_types} (rtid, content_type) VALUES (%d, '%s')", $rtid, $content_type);
  }
  cache_clear_all();
}

/**
 *  Delete relationship_type with the given parameters
 *
 *  @param $param
 *  Constraints which the deleted relationship types must satisfy.  Valid keys
 *  are 'rtid'.
 *
 *  @param $updating
 *  If set to TRUE, the query is treated as a delete before an insert, and the
 *  existing relationships defined with this relationship type are not deleted.
 *  Default value is FALSE.
 *
 *  @return
 *  nothing
 */
function user_related_content_delete_relationship_type($params, $updating = FALSE) {  
  db_query('DELETE FROM {user_related_content_relationship_types} WHERE rtid = %d', $params['rtid']);
  db_query('DELETE FROM {user_related_content_relationship_type_content_types} WHERE rtid = %d', $params['rtid']);
  if(!$updating) {
    db_query('DELETE FROM {user_related_content_relationships} WHERE rtid = %d', $params['rtid']);
  }
  cache_clear_all();
}

/**
 *  Get relationship_types
 *
 *  @param $param
 *  Constraints which the returned relationship types must satisfy.  Valid keys
 *  are 'uid', 'nid', 'rtid' and 'content_type'.
 *
 *  @param $content_types
 *  If set to TRUE, then set the ->content_types member on the returned
 *  relationship_type objects.  The default value is FALSE.
 *
 *  @return
 *  Array of relationship_type objects satisfying the $param constraints
 */
function user_related_content_get_relationship_types($params = NULL, $content_types = FALSE) {

  if(isset($params['count'])){
    $query = 'SELECT rt.*, COUNT('.$params['count']['table'].'.'.$params['count']['column'].') AS count ';
  }
  else {
    $query  = 'SELECT DISTINCT rt.* ';
  }
  $query .= 'FROM {user_related_content_relationship_types} rt';
  $query .= _user_related_content_param_sql($params, 'rt');

  if(isset($params['count'])) {
    unset($params['count']);
  }
  
  $result = db_query($query, $params);
  
  $relationship_types = array();
  while ($relationship_type = db_fetch_object($result)) {
    if ($content_types) {
      $ct_result = db_query('SELECT content_type FROM {user_related_content_relationship_type_content_types} WHERE rtid = %d', $relationship_type->rtid);
      while ($content_type = db_fetch_object($ct_result)) {
        $relationship_type->content_types[] = $content_type->content_type;
      }
    }
    $relationship_types[] = $relationship_type;
  }

  return $relationship_types;
}


/**
 *  Return SQL constraints to satisfy $param
 *
 *  @param $param
 *  Constraints which the returned SQL must satisfy.  Valid keys
 *  are 'uid', 'nid', 'rtid' and 'content_type'.
 *
 *  @param $main_table
 *  The alias of the main table being SELECT'ed.  Valid values are 'r', 'rt',
 *  and 'rtct'.
 *
 *  @param $do_join
 *  If false no tables are JOINed in the returned SQL. Useful for delete
 *  queries.  The default is TRUE (for SELECT queries).
 *
 *  @param $aliased
 *  If true then the column names in the original query being appended are
 *  aliased, and any JOINEDed tables and WHERE statements should also be
 *  aliased.  The default is TRUE.
 *
 *  @return
 *  SQL containing JOIN and WHERE statedments to be appended to the original
 *  query so that it may satisfy the passed $param constraints.
 */
function _user_related_content_param_sql($param, $main_table, $do_join = TRUE, $aliased = TRUE) {

  $tables = array(
  	'r' => 'user_related_content_relationships', 
  	'rt' => 'user_related_content_relationship_types', 
  	'rtct' => 'user_related_content_relationship_type_content_types',
  	'n' => 'node',
  	'u' => 'user'
  );

  $needed_tables = array();
  $where = array();
  $group_by = '';
  
  if (isset($param['uid'])) {
    $needed_tables[] = 'r';
    $where[] = 'uid = '.$param['uid'];
  }

  if (isset($param['rtid'])) {
    $needed_tables[] = $main_table;
    $where[] = 'rtid = '.$param['rtid'];
  }

  if (isset($param['nid'])) {
    $needed_tables[] = 'r';
    $where[] = 'nid = '.$param['nid'];
  }

  if (isset($param['content_type'])) {
    $needed_tables[] = 'rtct';
    $where[] = "content_type = '".$param['content_type']."'";
  }

  if (isset($param['count'])) {
    $needed_tables[] = $param['count']['table'];
    $group_by = ' GROUP BY '.$main_table.'.rtid';
  }
  
  $sql = '';
  
  if ($do_join) {
    foreach (array_unique($needed_tables) AS $tbl_alias) {
      if ($tbl_alias != $main_table) {
        $sql .= ' INNER JOIN {'.$tables[$tbl_alias].'} '.$tbl_alias.' ON '.$tbl_alias.'.rtid = '.$main_table.'.rtid';
      }
    }
  }
  
  if (count($where) > 0) {
    if ($aliased) {
      for ($i = 0; $i < count($where); $i++) {
        $where[$i] = $needed_tables[$i].'.'.$where[$i];
      }
    }
    $sql .= ' WHERE '.implode(' AND ', $where);
  }
  
  return $sql.$group_by;
}

/**
 *  return the node id if the current page is a node view page
 */
function _user_related_content_current_nid() {
  if (arg(0) == 'node' && is_numeric(arg(1))) {
    return arg(1);
  }  
  return 0;
}