
AUTHOR

  Drupal username: ejort (Eric Orton)
  Email: eric @ mineralsprings.net.au

DESCRIPTION

  A module to maintain relationships between users and content.

FEATURES

  o define arbitrary 'relationship types'
  o create relationships between users and nodes of $relationship_type list
    users related to the current node in a block (1 per relationship type which
    applies to the current node)
  o list nodes relating to a user in a local task on the users page
  o adds links for maintaing your relationships to nodes, eg/ add to list
    'photos of me' | remove from list 'articles mentioning me'

USES

  Useful for maintaining a list of people in each photo (and for linking to
  photos of a user from their user page) or for maintaining a list of people
  mentioned in an article (and linking to all the articles mentioning a user
  from their user page).

  This module provides a (hopefully) fairly comprehensive API which can be used
  by future modules to...
  
    o maintain a list of users who have edited an item (the author is probably
      going to attempt this)
    o read image IPTC data and use the person field to add a relationship
      between users and the image
    o maintain any other sort of user content relationships you can think of

INSTALLATION & CONFIGURATION

  o Create the SQL tables. This depends a little on your system, but the most
    common method is:
      mysql -u username -ppassword drupal < user_related_content.mysql
  o Copy 'user_related_content.module' to the 'modules/' directory
  o Enable the 'user_related_content' module in 'admin/modules'
  o Enable 'edit user_related_content relationship types', 'access
    user_related_content relationships' and 'edit own user_related_content
    relationships' permissions for the appropriate roles.
  o Visit 'admin/user_related_content' and create relationship types for your
    site

STATUS

  No known bugs, though the API may change slightly in the future...

TODO

  o More testing
  o Tag version for 4.6
  o Test with HEAD in preparation for 4.7

CHANGELOG

 29/7/05
  o First added to Drupal contrib cvs